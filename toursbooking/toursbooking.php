<?php
/*
Plugin Name: Booking-Maverik
Plugin URI: https://bookingmaverik.com/
Description: My first plugin
Version: 1.0
Author: Maverik
Author URI: https://maverik/wordpress-plugins/
License: GPLv2 or later
Text Domain: toursbooking
*/

if( !defined('ABSPATH') ) {
    die;
}

class ToursBooking {

    public function register() {

        //register post type
        add_action( 'init', array( $this, 'custom_post_type' ) );

        //add extra fields
        add_action( 'add_meta_boxes', array( $this, 'my_extra_fields' ) );

        //save extra fields
        add_action( 'save_post',  array( $this, 'my_extra_fields_update' ) );

        //enqueue admin
        add_action( 'admin_enqueue_scripts', array( $this,'enqueue_admin' ) );

        //enqueue front
        add_action( 'wp_enqueue_scripts', array( $this,'enqueue_front' ) );

        //load template
        add_filter( 'template_include', array( $this, 'tour_template') );

        //add custom columns
        add_filter('manage_tour_posts_columns' , array($this,'custom_post_type_columns'));
        add_action( 'manage_tour_posts_custom_column' , array($this,'fill_custom_post_type_columns'), 10, 2 );

    }

    public function custom_post_type_columns($columns) {

        // Remove Author and Comments from Columns and Add custom column 1, custom column 2 and Post Id
        unset(
            $columns['wpseo-score'],
            $columns['wpseo-title'],
            $columns['wpseo-metadesc'],
            $columns['wpseo-focuskw']
        );
        return array(
            'cb' => '<input type="checkbox" />',
            'title' => __('Заголовок'),
            'Country' => __('Страна'),
            'DateFrom' => __('Дата Тура Начало'),
            'DateTo' => __('Дата Тура Конец'),
            'Prize' => __('Цена'),
        );
        //return $columns;
    }

    public function fill_custom_post_type_columns( $column, $post_id ) {
        
        // Fill in the columns with meta box info associated with each post
        switch ( $column ) {
            case 'Country' :
                echo get_post_meta($post_id, 'select', 1);
                break;
            case 'DateFrom' :
                echo get_post_meta($post_id, 'the_date', 1);
                break;
            case 'DateTo' :
                echo get_post_meta($post_id, 'the_date2', 1);
                break;
            case 'Prize' :
                if( get_post_meta($post_id, 'prize', 1) != '' ) {echo get_post_meta($post_id, 'prize', 1).'$';}
                break;

        }
    }

    public function my_extra_fields() {
        add_meta_box( 'extra_fields', 'Дополнительные поля',  array( $this, 'extra_fields_box_func' ), 'tour', 'normal', 'high'  );
    }

    public function extra_fields_box_func( $post ){
        ?>
        <label>Страна  <select name="extra[select]">
                <?php $sel_v = get_post_meta($post->ID, 'select', 1); ?>
                <option value="0">----</option>
                <option value="Бали" <?php selected( $sel_v, 'Бали' )?> >Бали</option>
                <option value="Турция" <?php selected( $sel_v, 'Турция' )?> >Турция</option>
                <option value="Египет" <?php selected( $sel_v, 'Египет' )?> >Египет</option>
                <option value="Анталия" <?php selected( $sel_v, 'Анталия' )?> >Анталия</option>
            </select>
        </label>
        <p>
            <label> Цена $ <input type="number" min="0" name="extra[prize]" value="<?php echo get_post_meta($post->ID, 'prize', 1); ?>" style="width: 88px;" /></label>
        </p>

        <p>
            <label>Дата Тура Начало <input type="date" name="extra[the_date]" value="<?php echo get_post_meta($post->ID, 'the_date', 1); ?>" /> </label>
        </p>
        <p>
            <label>Дата Тура Конец <input type="date" name="extra[the_date2]" value="<?php echo get_post_meta($post->ID, 'the_date2', 1); ?>" /> </label>
        </p>

        <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
        <?php
    }

    public function my_extra_fields_update( $post_id ){
        if(
            empty( $_POST['extra'] )
            || ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
            || wp_is_post_autosave( $post_id )
            || wp_is_post_revision( $post_id )
        )
            return false;

        $_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] );
        foreach( $_POST['extra'] as $key => $value ){
            if( empty($value) ) {
                delete_post_meta( $post_id, $key );
                continue;
            }

            update_post_meta( $post_id, $key, $value );
        }

        return $post_id;
    }

    static function activation() {

        //update rewrite rules
        flush_rewrite_rules();
    }

    static function deactivation() {

        //update rewrite rules
        flush_rewrite_rules();
    }

    public function enqueue_admin() {
        wp_enqueue_style('toursBookingStyle', plugins_url( '/assets/admin/styles.css', __FILE__ ));
        wp_enqueue_script('toursBookingScript', plugins_url( '/assets/admin/script.js', __FILE__ ));
        wp_enqueue_script('jquery-ui-datepicker');
        wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
        wp_enqueue_style('jquery-ui');
    }

    public function enqueue_front() {
        wp_enqueue_style('toursBookingStyle', plugins_url( '/assets/front/styles.css', __FILE__ ));
        wp_enqueue_script('toursBookingScript', plugins_url( '/assets/front/script.js', __FILE__ ));
    }

    public function tour_template($template) {
        if( is_post_type_archive( 'tour' ) ) {
            $theme_files = array( 'archive-tour.php', 'toursbooking/archive-tour.php');
            $exist_in_theme = locate_template( '$theme_files', false );
            if( $exist_in_theme != '' ) {
                return $exist_in_theme;
            } else {
                return plugin_dir_path( __FILE__ ).'templates/archive-tour.php';
            }
        }
        return $template;
    }

    public function custom_post_type() {
        $labels = array(
            'name'                  => _x( 'Все Туры', 'Post type general name', 'toursbooking' ),
            'all_items'              => _x( 'Все Туры', 'toursbooking' ),
            'singular_name'         => _x( 'Тур', 'Post type singular name', 'toursbooking' ),
            'menu_name'             => _x( 'Туры', 'Admin Menu text', 'toursbooking' ),
            'add_new'               => __( 'Новый Тур', 'toursbooking' ),
            'add_new_item'          => __( 'Добавить Новый Тур', 'toursbooking' ),
            'featured_image'        => _x( 'Tour Cover Image', 'toursbooking' ),
            'set_featured_image'    => _x( 'Set cover image', 'toursbooking' ),
            'remove_featured_image' => _x( 'Remove cover image', 'toursbooking' ),
            'use_featured_image'    => _x( 'Use as cover image', 'toursbooking' ),
            );
        register_post_type('tour', array(
            'public' => true,
            'has_archive' => true,
            'rewrite' => array( 'slug' => 'tours' ),
            'labels' => $labels,
            'supports'=> array( 'title', 'editor', 'thumbnail' ),
        ));
    }
}

if( class_exists('ToursBooking') ) {
    $toursBooking = new ToursBooking();
    $toursBooking->register();
}

register_activation_hook( __FILE__, array( $toursBooking, 'activation' ) );
register_deactivation_hook( __FILE__, array( $toursBooking, 'deactivation' ) );

